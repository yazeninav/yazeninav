async function cleanConsole() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.clear();
            resolve()
        }, 150)
    });
}

function loading(fn) {
    return Promise.resolve(fn());
}

async function asyncFn() {
    const chars = ['|', '/', '-', '\\'];

    console.clear();

    let flag = true;

    setTimeout(() => {
        flag = false
    }, 5000)

    while (flag) {
        for (let char of chars) {
            console.log(char);
            await cleanConsole();
        }
    }
    return Promise.resolve();
}

loading(asyncFn).then(value => console.log('I replace loading symbols'));