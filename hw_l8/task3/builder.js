function Builder(...args) {
    const builderArgs = args;

    return function FunctionConstructor(...args) {

        const fillObjectFields = () => {
            let newObj = {};
            for (let i = 0; i < args.length; i++) {
                newObj[builderArgs[i]] = args[i];
            }
            return newObj
        };

        return fillObjectFields();
    }
}

let Person = new Builder('name', 'age');
let person = new Person('Varia', 10);
console.log(person)

let NewPerson = new Builder('name', 'age', 'address', 'sex');
let newPerson = new NewPerson('Varia', 29, 'Mogilev', 'women');
console.log(newPerson)


let House = new Builder('floors', 'apartments', 'type', 'address');
let house29 = new House(10, 80, 'brick', 'Green str., 29');
console.log(house29)

let Apartment = new Builder('house', 'number', 'rooms');
let apartment42 = new Apartment(house29, 42, 3);

console.log(apartment42)


