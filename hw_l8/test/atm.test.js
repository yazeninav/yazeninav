const assert = require('assert')
const Card = require('../task1/card')
const Atm = require('../task1/atm')
const CreditCard = require('../task1/credit-card')
const DebitCard = require('../task1/debit-card')

describe('atm.test', () => {
    it('should take card instance of card', () => {
        const atm = new Atm();
        const card = new Card("Varya", 500);
        atm.card = card;
        assert.equal(atm._card, card)
    });
    it('should take card instance of card', () => {
        const atm = new Atm();
        const card = new CreditCard("Varya", 500);
        atm.card = card;
        assert.equal(atm._card, card)
    });
    it('should take card instance of card', () => {
        const atm = new Atm();
        const card = new DebitCard("Varya", 500);
        atm.card = card;
        assert.equal(atm._card, card)
    });
    it('should not take card differ then instance of card', () => {
        const atm = new Atm();
        const card = "Card";
        atm.card = card;
        assert.equal(atm._card, undefined)
    });
    it('should supplement balance of card by 100', () => {
        const atm = new Atm();
        const card = new Card("varia",500);
        atm.card = card;
        atm.supplementBalance(100)
        assert.equal(card.balance, 600)
    });
    it('should withdraw balance of card by 100', () => {
        const atm = new Atm();
        const card = new Card("varia",500);
        atm.card = card;
        atm.withdrawBalance(100)
        assert.equal(card.balance, 400)
    });
});
