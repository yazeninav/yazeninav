const assert = require('assert')
const DebitCard = require('../task1/debit-card')

describe('debit-card.test', () => {
    it('should supplement balance of card by 100', () => {
        const card = new DebitCard("Varya", 500);
        card.supplementBalance(100)
        assert.equal(card.balance, 600)
    });
    it('should withdraw balance of card by 100', () => {
        const card = new DebitCard("Varya", 500);
        card.withdrawBalance(100)
        assert.equal(card.balance, 400)
    });
})