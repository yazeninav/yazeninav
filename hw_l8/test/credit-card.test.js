const assert = require('assert')
const CreditCard = require('../task1/credit-card')

describe('credit-card.test', () => {
    it('should supplement balance of card by 100', () => {
        const card = new CreditCard("Varya", 500);
        card.supplementBalance(100)
        assert.equal(card.balance, 600)
    });
    it('should withdraw balance of card by 100', () => {
        const card = new CreditCard("Varya", 500);
        card.withdrawBalance(100)
        assert.equal(card.balance, 400)
    });
    it('should not withdraw balance of card if result les then 0', () => {
        const card = new CreditCard("Varya", 500);
        card.withdrawBalance(600)
        assert.equal(card.balance, 500)
    });
})