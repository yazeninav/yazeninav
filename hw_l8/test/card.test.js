const assert = require('assert')
const Card = require('../task1/card')


describe('card.js Test', () => {
    it('should create Card object with owner name=test and balance=100', () => {
        const card = new Card('test',100)
        assert.equal(card.name,'test');
        assert.equal(card.balance,100);
    });
    it('should create Card object with owner name=test and default balance=0', () => {
        const card = new Card('test')
        assert.equal(card.name,'test');
        assert.equal(card.balance,0);
    });
    it('should increase balance by 50', () => {
        const card = new Card('test',100)
        card.supplementBalance(50)
        assert.equal(card.balance,150);
    });
    it('should decrease balance by 50', () => {
        const card = new Card('test',100)
        card.withdrawBalance(50)
        assert.equal(card.balance,50);
    });
    it('should return converted balance balance', () => {
        const card = new Card('test',100)
        const result = card.getConvertedBalance(1.1)
        assert.deepEqual(result,110);
    });
});