const Card = require('./card')

class DebitCard extends Card {
    constructor(name, balance) {
        super(name, balance)
    }
    withdrawBalance (amount) {

        if (this._balance - amount < 0) {
            console.log("not enough money")
        } else {
            this._balance -= amount;
        }
    }
}

module.exports=DebitCard


