let Card = require('./card');

class Atm {
    _card;

    set card(card) {
        if (card instanceof Card) {
            this._card = card;
        } else {
            console.log("this type of card does not supported")
        }
    }

    withdrawBalance(amount) {
        if (this._card) {
            this._card.withdrawBalance(amount);
        }
    }

    supplementBalance(amount) {
        if (this._card) {
            this._card.supplementBalance(amount);
        }
    }

}

module.exports = Atm;