const Card = require('./card')

class CreditCard extends Card {
    constructor(name, balance) {
        super(name, balance);
    }

    withdrawBalance(amount) {
        if (amount > this._balance) {
            console.log("not enough money")
        } else {
            this._balance -= amount;
        }
    }
}

module.exports = CreditCard

