class Card {
    constructor(name, balance) {
        this._name = name;
        this._balance = balance || 0;
    }

    get balance() {
        return this._balance;
    }
    get name() {
        return this._name;
    }

    withdrawBalance (amount) {
        this._balance -= amount;
    }

    supplementBalance (amount) {
        if (typeof amount === 'number') {
            this._balance += amount;
        } else {
            console.log('incorrect input type')
        }
    }
    getConvertedBalance (convertCoefficient) {
        return convertCoefficient * 1000 * this._balance / 1000;
    }
}
module.exports = Card


