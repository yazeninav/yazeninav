const PIZZA_TOPPINGS = require('./pizza-toppings')

module.exports = class Pizza {

    constructor(size) {
        this._size = size;
        this._toppings = [];
    }

    get toppings() {
        return this._toppings;
    }

    get size() {
        return this._size;
    }

    set size(size) {
        this._size = size;
    }

    addTopping(topping) {
        this.toppings.push(topping)
    }

    removeTopping(topping) {
        let index = this.toppings.indexOf(topping);
        if (index !== -1) this.toppings.splice(index, 1);
    }

    isVegetarian() {
        return !(this.toppings.includes(PIZZA_TOPPINGS.MEAT) || this.toppings.includes(PIZZA_TOPPINGS.SALAMI))
    }

}