const Pizza = require('./pizza')
const PIZZA_SIZE = require('./pizza-sizes')
const PIZZA_TOPPINGS = require('./pizza-toppings')
const PizzaCalculator = require('./pizza-calculator')


const pizza = new Pizza(PIZZA_SIZE.SMALL);
pizza.addTopping(PIZZA_TOPPINGS.MEAT);
pizza.addTopping(PIZZA_TOPPINGS.CHEESE);
pizza.addTopping(PIZZA_TOPPINGS.SALAMI);
pizza.addTopping(PIZZA_TOPPINGS.SALAD);

const pizzaCalculator = new PizzaCalculator(pizza);
console.log(pizza.isVegetarian())
console.log(pizzaCalculator.calculateCalories())
console.log(pizzaCalculator.calculatePrice())

pizza.removeTopping(PIZZA_TOPPINGS.MEAT)
console.log(pizza.isVegetarian())
pizza.removeTopping(PIZZA_TOPPINGS.SALAMI)
console.log(pizza.isVegetarian())

console.log(pizzaCalculator.calculateCalories())
console.log(pizzaCalculator.calculatePrice())

console.log()

pizza.setSize = PIZZA_SIZE.BIG
pizza.addTopping(PIZZA_TOPPINGS.MEAT);
pizza.addTopping(PIZZA_TOPPINGS.MEAT);
pizza.addTopping(PIZZA_TOPPINGS.MEAT);
pizza.addTopping(PIZZA_TOPPINGS.MEAT);

console.log(pizza.isVegetarian())

console.log(pizzaCalculator.calculateCalories())
console.log(pizzaCalculator.calculatePrice())





