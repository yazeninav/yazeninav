module.exports = {
    SMALL: {
        price: 50,
        calories: 20
    },

    BIG: {
        price: 100,
        calories: 40
    }
}