module.exports = class PizzaCalculator {

    constructor(pizza) {
        this._pizza = pizza;
    }

    set pizza(pizza) {
        this._pizza = pizza;
    }

    get pizza(){
        return this._pizza;
    }

    calculatePrice() {
        return this.pizza.size.price + this.pizza.toppings
            .reduce((previousValue, currentValue) => previousValue.price + currentValue.price, 0)
    }

    calculateCalories() {
        return this.pizza.size.calories + this.pizza.toppings
            .reduce((previousValue, currentValue) => previousValue.calories + currentValue.calories, 0)
    }
}