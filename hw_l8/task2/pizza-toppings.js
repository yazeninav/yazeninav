module.exports = {
    CHEESE: {
        price: 10,
        calories: 10
    },

    SALAMI: {
        price: 15,
        calories: 20
    },

    SALAD: {
        price: 10,
        calories: 20
    },

    MEAT: {
        price: 20,
        calories: 15
    }
}