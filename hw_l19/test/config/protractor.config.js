const yargs = require('yargs').argv;
exports.config = {

    params: {
        login: yargs.login,
        psw: yargs.psw
    },

    directConnect: true,

    framework: 'mocha',

    specs: [
        '../specs/*.js'
    ],
    multiCapabilities: [{
        'browserName': 'chrome',

    }, {
        'browserName': 'firefox',
    }],
    // shardTestFiles: yargs.instances > 1,
    // maxInstances: yargs.instances || 2,
    baseUrl: 'localhost',

    mochaOpts: {
        reporter: 'spec',
        timeout: 70000
    }
};