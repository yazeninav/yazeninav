const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Collection = require("../base_elements/base_collection");
const Element = require("../base_elements/base_element");
const  MainPage= require("../main_page/main_page");
const yargs = require('yargs').argv;


class PswPage extends BasePage {
    constructor() {
        super();
        this.element = new Element("Fill psw", "#passp-field-passwd");
    };

    async fillPsw() {
        await this.element.fillText(browser.params.psw)
        return new MainPage();
    }
};

module.exports = PswPage;