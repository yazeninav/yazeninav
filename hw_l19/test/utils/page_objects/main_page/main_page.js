const BasePage = require("../base_page/base_page");
const Element = require("../base_elements/base_element");

class MainPage extends BasePage {
    constructor() {
        super();
        this.newEmainButton = new Element("new email button", ".mail-ComposeButton.js-main-action-compose");
        this.toField = new Element("to field", ".composeYabbles");
        this.subjectField = new Element("subject field", ".composeTextField.ComposeSubject-TextField");
        this.msgField = new Element("msg field", ".cke_wysiwyg_div.cke_reset.cke_enable_context_menu.cke_editable.cke_editable_themed.cke_contents_ltr.cke_htmlplaceholder");
        this.sendButton = new Element("send button", ".control.button2.button2_view_default.button2_tone_default.button2_size_l.button2_theme_action.button2_pin_circle-circle.ComposeControlPanelButton-Button.ComposeControlPanelButton-Button_action");
        this.emailCount = new Element("email count", ".mail-LabelList-Item_count");
        this.refreshButton = new Element("refresh button", ".mail-ComposeButton-Refresh.js-main-action-refresh.ns-action");
        this.returnLink = new Element("return link", ".control.link.link_theme_normal.ComposeDoneScreen-Link");
    };

    async fillFromField() {
        await this.toField.fillText("variatest@yandex.by")
    }

    async fillSubject() {
        await this.subjectField.fillText("test")
    }

    async fillMsg() {
        await this.msgField.fillText("test")
    }

};

module.exports = MainPage;