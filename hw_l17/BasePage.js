let webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until,
    Key = webdriver.Key

let driver = new webdriver.Builder()
    .withCapabilities(webdriver.Capabilities.chrome())
    .build();

class BasePage {

    constructor() {
        global.driver = driver;
        global.By = By;
        global.until = until;
        global.Key = Key;
    }

    async open(url) {
        return driver.get(url);
    }

    async exit() {
        driver.quite()
    }
}

module.exports = BasePage;