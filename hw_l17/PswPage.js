let BasePage = require('./BasePage')

class PswPage extends BasePage {

    async fillPsw() {
        let locator = By.id('passp-field-passwd');
        await driver.wait(until.elementLocated(locator), 10000);
        await driver.findElement(locator).sendKeys('Test123', Key.RETURN);
        return require('./MainPage')
    }

}

module.exports = new PswPage();