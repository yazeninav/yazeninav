let BasePage = require('./BasePage')

class WelcomePage extends BasePage {

    clickEnterButton() {
        driver.findElement(By.className('button2 button2_size_mail-big button2_theme_mail-white button2_type_link HeadBanner-Button HeadBanner-Button-Enter with-shadow')).click();
        return require('./LoginPage');
    }

    async open() {
        return super.open('https://mail.yandex.by/');
    }
}

module.exports = new WelcomePage();