let BasePage = require('./BasePage')

class LoginPage extends BasePage{

    async fillLogin() {
        let locator = By.id('passp-field-login');
        await driver.wait(until.elementLocated(locator), 10000);
        await driver.findElement(locator).sendKeys('variatest', Key.RETURN);
        return require('./PswPage');
    }

}

module.exports = new LoginPage();