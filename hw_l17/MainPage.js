let BasePage = require('./BasePage')

class MainPage extends BasePage {

    async clickNewEmailButton() {
        let locator = By.className('mail-ComposeButton js-main-action-compose');
        await driver.wait(until.elementLocated(locator), 10000);
        driver.findElement(By.className('mail-ComposeButton js-main-action-compose')).click();
    }

    async fillFromField() {
        let locator = By.className('composeYabbles');
        await driver.wait(until.elementLocated(locator), 10000);
        await driver.findElement(locator).sendKeys('variatest@yandex.by', Key.RETURN);
    }

    fillSubjectField() {
        driver.findElement(By.className('composeTextField ComposeSubject-TextField')).sendKeys('test', Key.RETURN);
    }

    async fillMsgText() {
        let locator = By.className('cke_wysiwyg_div cke_reset cke_enable_context_menu cke_editable cke_editable_themed cke_contents_ltr');
        await driver.actions().keyDown(Key.SHIFT).perform();
        let el = await driver.findElement(locator);
        await el.sendKeys('test')
        await driver.actions().keyUp(Key.SHIFT).perform();

        driver.executeScript("arguments[0].style.backgroundColor = '" + "red" + "'", el)
    }

    async clickSendEmail() {
        let locator = By.className('control button2 button2_view_default button2_tone_default button2_size_l button2_theme_action button2_pin_circle-circle ComposeControlPanelButton-Button ComposeControlPanelButton-Button_action');
        let el = await driver.findElement(locator);
        driver.executeScript("arguments[0].click()", el)
    }

}

module.exports = new MainPage();