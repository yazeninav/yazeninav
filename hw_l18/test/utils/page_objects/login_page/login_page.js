const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Collection = require("../base_elements/base_collection");
const Element = require("../base_elements/base_element")
const PswPage = require("../psw_page/psw_page")

class LoginPage extends BasePage {
    constructor() {
        super();
        this.element = new Element("Fill login", "#passp-field-login");
    };

    async fillLogin() {
        await this.element.fillText('variatest')
        return new PswPage();
    }
};

module.exports = LoginPage;