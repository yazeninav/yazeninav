const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Collection = require("../base_elements/base_collection");
const Element = require("../base_elements/base_element")
const LoginPage = require("../login_page/login_page")

class WelcomePage extends BasePage {
    constructor() {
        super();
        this.url = "https://mail.yandex.by/";
        this.element = new Element("Enter button", ".button2.button2_size_mail-big.button2_theme_mail-white.button2_type_link.HeadBanner-Button.HeadBanner-Button-Enter.with-shadow");
    };

    open() {
        return super.open(this.url);
    };

    async clickEnterButton() {
        await this.element.click();
        return new LoginPage();
    }
};

module.exports = WelcomePage;