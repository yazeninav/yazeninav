const WelcomePage = require("./welcome_page/welcome_page");
const LoginPage = require("./login_page/login_page");
const BasePage = require("./base_page/base_page");
const PswPage = require("./psw_page/psw_page");

class PageFactory {
     static getPage(pageName) {
        switch (pageName) {
            case "Welcome":
                return new WelcomePage();
            case "Login":
                return new LoginPage();
            case "Psw":
                return new PswPage();
            default:
                return new BasePage();
        }
        ;
    };
};

module.exports = PageFactory;