const expect = require("chai").expect;
const PageFactory = require("../utils/page_objects/pageFactory");
const EC = protractor.ExpectedConditions;
describe("Main page send email", function () {

    beforeEach(function () {
        browser.ignoreSynchronization = true;
        return browser.manage().window().maximize();
    });

    it("should increase email count by 1", async function () {
        let welcomePage = await PageFactory.getPage("Welcome")
        await welcomePage.open();
        let loginPage = await welcomePage.clickEnterButton();
        await loginPage.wait(2000);
        let pswPage = await loginPage.fillLogin();
        await pswPage.wait(2000);
        let mainPage = await pswPage.fillPsw();
        await mainPage.wait(2000);

        let countBeforeSending = await mainPage.emailCount.getText()

        await mainPage.newEmainButton.click();
        await mainPage.wait(2000);
        await mainPage.fillFromField();
        await mainPage.fillSubject();
        await mainPage.fillMsg();
        await mainPage.sendButton.click();
        await mainPage.wait(2000);
        await mainPage.returnLink.click();
        await mainPage.wait(30000);
        await mainPage.refreshButton.click();
        await mainPage.wait(2000);

        let result = await mainPage.emailCount.getText();

        expect(result).to.be.equal('' + ++countBeforeSending);
    });

});