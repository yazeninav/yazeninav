const SEX_TYPE = 'Female';
const STRING_TYPE = "string";
const NUMBER_TYPE = "number";
const BOOLEAN_TYPE = "boolean";

const students = [
    {
        name: 'Vasya Pupkin',
        age: 17,
        sex: 'Male',
        isMarried: false,
        city: 'Mogilev',
    },
    {
        name: 'Zoya Petrovna',
        age: 23,
        sex: 'Female',
        isMarried: true,
        city: 'Mogilev',
    },
    {
        name: 'Petr Ivanov',
        age: 30,
        sex: 'Male',
        isMarried: true,
        city: 'Minsk',
    },
    {
        name: 'Vitali Ivanov',
        age: 19,
        sex: 'Male',
        isMarried: false,
        city: 'Vitebsk',
    },
    {
        name: 'Lavrenti Sakalov',
        age: 20,
        sex: 'Male',
        isMarried: true,
        city: 'Brest',
    },
    {
        name: 'Olga Sakalova',
        age: 23,
        sex: 'Female',
        isMarried: true,
        city: 'Grodno',
    },
]

function print(value) {
    console.log(value)
}

function checkInputType(inputValue, requiredInputType) {
    const inputType = typeof inputValue;

    if (inputType !== requiredInputType) {
        console.log('incorrect input type, required: ' + requiredInputType + ' input: ' + inputType)
        return false
    }

    return true
}

function printStudentsByCity(city) {
    if (checkInputType(city, STRING_TYPE)) {
        students
            .filter(student => student.city === city)
            .forEach(print)
    }
}

print("find by city")
printStudentsByCity('Brest')

function printStudentsByAge(age) {
    if (checkInputType(age, NUMBER_TYPE)) {
        students
            .filter(student => student.age <= age)
            .forEach(print)
    }
}

console.log('find by age')
printStudentsByAge(25)

function printSortedStudentsNames() {
    students
        .map(student => student.name)
        .sort()
        .forEach(print)
}

console.log("sorted names")
printSortedStudentsNames();

function getStudentsByStatus(isMarried) {
    if (checkInputType(isMarried, BOOLEAN_TYPE)) {
        students
            .filter(student => student.sex === SEX_TYPE && student.isMarried === isMarried)
            .forEach(print)
    }
}

console.log('female students by status')
getStudentsByStatus(true)

function printStudentsBySexAndAge(sex, age) {
    if (checkInputType(sex, STRING_TYPE) && checkInputType(age, NUMBER_TYPE)) {
        students
            .filter(student => student.sex === sex && student.age === age)
            .forEach(print)
    }
}

print('student by sex and age')
print(printStudentsBySexAndAge('Male', 17));

const newStudent = {
    name: 'Varvara Yazenina',
    age: 29,
    sex: 'Male',
    isMaried: true,
    city: 'Mogilev'
}

students.unshift(newStudent);

print('new students');
print(students);

const printUniqueCity = function () {
    students
        .map(student => student.city)
        .filter(onlyUnique)
        .forEach(print)

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
}

print('unique cities names')
printUniqueCity()