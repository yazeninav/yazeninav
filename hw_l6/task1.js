 function createObject(arr) {

    const object = {};

    for (let i = 0; i < arr.length; i += 2) {
        if (arr [i + 1]) {
            object[arr[i]] = null;
        } else {
            object[arr[i]] = arr [i + 1];
        }
    }

    return object

}

const arr = ['1', 1, 'a', {hello: 'world'}, 'qwe'];

console.log(arr)
console.log(createObject(arr))