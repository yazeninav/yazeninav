import { Track } from "../../domain/track/track";
import * as _ from "lodash";
import { MusicLibrary } from "../../domain/music-library";
import { Model } from "../../domain/model";
import { Album } from "../../domain/album";
import { Artist } from "../../domain/artist";

export abstract class AbstractCrudService<T extends Model> {
  protected readonly musicLibrary: MusicLibrary;

  constructor(musicLibrary: MusicLibrary) {
    this.musicLibrary = musicLibrary;
  }

  create(entity: T): void {
    let repositoryName: string = this.getRepositoryName(entity);
    this.musicLibrary[repositoryName].push(entity);
  }

  delete(entity: T): void {
    _.remove(this.musicLibrary[this.getRepositoryName(entity)], (element: Model) => element.id === entity.id);
  }

  read(entity: T): T {
    for (let element of this.musicLibrary[this.getRepositoryName(entity)]) {
      if (element.id === entity.id) return element;
    }
    throw new Error(`Track with id:${entity.id} not found`);
  }

  update(entity: T): void {
    this.delete(entity);
    this.create(entity);
  }

  isTrack(entity: T): entity is T {
    return entity instanceof Track;
  }

  isAlbum(entity: T): entity is T {
    return entity instanceof Album;
  }

  isArtist(entity: T): entity is T {
    return entity instanceof Artist;
  }

  getRepositoryName(entity: T): string {
    if (this.isTrack(entity)) {
      return "tracks";
    } else if (this.isAlbum(entity)) {
      return "albums";
    } else return "artists";
  }
}
