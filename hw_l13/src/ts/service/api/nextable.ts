export interface Nextable {
  switchToNext(): void;
}
