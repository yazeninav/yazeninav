import { AbstractCrudService } from "../api/abstract-crud-service";
import { Artist } from "../../domain/artist";
import { MusicLibrary } from "../../domain/music-library";

export class ArtistServiceImpl extends AbstractCrudService<Artist> {
  constructor(musicLibrary: MusicLibrary) {
    super(musicLibrary);
  }
}
