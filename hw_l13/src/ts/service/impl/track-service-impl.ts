import { AbstractCrudService } from "../api/abstract-crud-service";
import { Track } from "../../domain/track/track";
import { MusicLibrary } from "../../domain/music-library";
import { Playable } from "../api/playable";
import { Pausable } from "../api/pausable";
import { TrackState } from "../../domain/track/track-state";
import { Nextable } from "../api/nextable";
import { Previousable } from "../api/previousable";

export class TrackServiceImpl extends AbstractCrudService<Track>
  implements Playable, Pausable, Nextable, Previousable {
  private performedTrack: Track | null;
  private timePassed: number;

  constructor(musicLibrary: MusicLibrary) {
    super(musicLibrary);
    this.performedTrack = null;
    this.timePassed = 0;
  }

  switchToPrevious(): void {
    this.switchCheck();

    if (this.performedTrack == null) {
      throw new Error("Track is not selected");
    }

    this.performedTrack.state = TrackState.STOP;

    let performedTrackIndex: number = this.getPerformedTrackIndex();
    if (performedTrackIndex > 0) {
      this.play(this.musicLibrary.tracks[--performedTrackIndex]);
    } else {
      this.play(this.musicLibrary.tracks[this.musicLibrary.tracks.length-1]);
    }
  }

  switchToNext(): void {
    this.switchCheck();

    if (this.performedTrack == null) {
      throw new Error("Track is not selected");
    }

    this.performedTrack.state = TrackState.STOP;

    let performedTrackIndex = this.getPerformedTrackIndex();
    if (performedTrackIndex < this.musicLibrary.tracks.length - 1) {
      this.play(this.musicLibrary.tracks[++performedTrackIndex]);
    } else {
      this.play(this.musicLibrary.tracks[0]);
    }
  }

  private switchCheck() {
    if (this.musicLibrary.tracks.length == 0) {
      throw new Error("No tracks");
    }
  }

  private getPerformedTrackIndex(): number {
    if (this.performedTrack == null) {
      throw new Error("Track is not selected");
    }

    return this.musicLibrary.tracks.findIndex(
      (value) => this.performedTrack == null ? false : this.performedTrack.id == value.id
    );
  }

  pause(): void {
    if (this.performedTrack == null) {
      throw new Error("Track is not selected");
    }
    if (this.performedTrack.state == TrackState.STOP) {
      throw new Error("track already stopped");
    }
    this.performedTrack.state = TrackState.STOP;
  }

  play(track: Track): void {
    if (this.performedTrack !== null && this.performedTrack.state !== TrackState.STOP) {
      throw new Error(`another track:${this.performedTrack.name} is playing`);
    }
    if (track.state == TrackState.PLAY) {
      throw new Error("track is already playing");
    }
    this.performedTrack = track;
    track.state = TrackState.PLAY;
    for (let i = this.timePassed; i < track.duration.fullDuration; i++) {
      setTimeout(() => {
        if (track.state == TrackState.STOP) return;
        if (this.timePassed <= track.duration.fullDuration) {
          console.log(`${track.text} time passed ${++this.timePassed}`);
        }
        if (this.timePassed === track.duration.fullDuration) {
          this.timePassed = 0;
        }
      }, 1000);
    }
  }
}
