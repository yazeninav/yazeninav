import { AlbumServiceImpl } from "./album-service-impl";
import { MusicLibrary } from "../../domain/music-library";
import { TrackServiceImpl } from "./track-service-impl";
import { ArtistServiceImpl } from "./artist-service-impl";
import { Artist } from "../../domain/artist";
import { Album } from "../../domain/album";
import { Track } from "../../domain/track/track";
import { Model } from "../../domain/model";

export class MusicLibraryServiceImpl {
  private albumServiceImpl: AlbumServiceImpl;
  private artistServiceImpl: ArtistServiceImpl;
  private trackServiceImpl: TrackServiceImpl;
  private musicLibrary: MusicLibrary;

  constructor(musicLibrary: MusicLibrary) {
    this.musicLibrary = musicLibrary;
    this.albumServiceImpl = new AlbumServiceImpl(musicLibrary);
    this.artistServiceImpl = new ArtistServiceImpl(musicLibrary);
    this.trackServiceImpl = new TrackServiceImpl(musicLibrary);
  }

  async find(entity: Model): Promise<Array<Artist | Album | Track>> {
    if (entity instanceof Artist) {
      return this.musicLibrary.tracks
        .filter((track) => {
        let ids = track.artists.map((artist) => artist.id);
        return ids.includes(entity.id);
        }
      );
    } else if (entity instanceof Album) {
      return this.musicLibrary.artists.filter((artist) =>
        artist.albums.map((album) => album.id).includes(entity.id)
      );
    } else {
      return this.musicLibrary.albums.filter((album) =>
        album.tracks.map((track) => track.id).includes(entity.id)
      );
    }
  }
}
