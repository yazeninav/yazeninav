import { Artist } from "../artist";
import { Duration } from "../duration";
import { TrackState } from "./track-state";
import { Model } from "../model";

export class Track extends Model {
  name: string;
  text: string;
  state: TrackState;
  artists: Array<Artist>;
  duration: Duration;
  mark: number;

  constructor(
    name: string,
    text: string,
    artist: Array<Artist>,
    duration: Duration,
    mark: number
  ) {
    super();
    this.name = name;
    this.text = text;
    this.state = TrackState.STOP;
    this.artists = [];
    this.duration = duration;
    this.mark = mark | 0;
  }
}
