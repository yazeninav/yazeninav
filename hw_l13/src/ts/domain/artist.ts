import { Album } from "./album";
import { Model } from "./model";

export class Artist extends Model {
  albums: Array<Album>;
  firstName: string;
  lastName: string;

  constructor(firstName: string, lastName: string) {
    super();
    this.albums = [];
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
