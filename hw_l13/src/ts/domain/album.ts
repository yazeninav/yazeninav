import { Track } from "./track/track";
import { Model } from "./model";

export class Album extends Model {
  name: string;
  releaseDate: Date;
  tracks: Array<Track>;

  constructor(releaseDate: Date, tracks: Array<Track>, name: string) {
    super();
    this.releaseDate = releaseDate;
    this.tracks = tracks;
    this.name = name;
  }
}
