export class Duration {
  private min: number;
  private seconds: number;
  private readonly _fullDuration: number;

  constructor(min: number, seconds: number) {
    this.min = min | 0;
    this.seconds = seconds | 0;
    this._fullDuration = min * 60 + seconds;
  }

  get fullDuration(): number {
    return this._fullDuration;
  }
}
