import { Album } from "../../ts/domain/album";
import { Track } from "../../ts/domain/track/track";
import { Duration } from "../../ts/domain/duration";
import { Artist } from "../../ts/domain/artist";

export class TestHelper {

  static getAlbum(): Album {
    return new Album(new Date(), [], "test");
  }

  static getTrack(): Track {
    return new Track("test", "test", [], new Duration(1, 0), 5);
  }

  static getArtist(): Artist {
    return new Artist("test", "test");
  }
}