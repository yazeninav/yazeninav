import "mocha";
import { expect } from "chai";
import { TestHelper } from "./test-helper";
import { MusicLibrary } from "../../ts/domain/music-library";
import { Artist } from "../../ts/domain/artist";
import { Album } from "../../ts/domain/album";
import { Track } from "../../ts/domain/track/track";
import { MusicLibraryServiceImpl } from "../../ts/service/impl/music-library-service-impl";


describe("Album service test", () => {

  let artist: Artist;
  let album: Album;
  let track: Track;
  let musicLibrary: MusicLibrary;
  let musicLibraryServiceImpl: MusicLibraryServiceImpl;

  beforeEach(done => {
    artist = TestHelper.getArtist();
    album = TestHelper.getAlbum();
    track = TestHelper.getTrack();
    artist.albums = [album];
    album.tracks = [track];
    track.artists = [artist];

    musicLibrary = MusicLibrary.create();
    musicLibrary.tracks = [track];
    musicLibrary.albums = [album];
    musicLibrary.artists = [artist];

    musicLibraryServiceImpl = new MusicLibraryServiceImpl(musicLibrary);

    done();
  });


  describe("#find", function() {
    it("find tracks by artist", () => {
      return musicLibraryServiceImpl.find(artist).then(result => {
        expect(result[0].id).to.equal(musicLibrary.tracks[0].id);
      });
    });
    it("find artists by album", () => {
      return musicLibraryServiceImpl.find(album).then(result => {
        expect(result[0].id).to.equal(musicLibrary.artists[0].id);
      });
    });
    it("find albums by track", () => {
      return musicLibraryServiceImpl.find(track).then(result => {
        expect(result[0].id).to.equal(musicLibrary.albums[0].id);
      });
    });
  });

});