import { User } from './user'

export class Observer {
    user: User

    constructor(user: User) {
        this.user = user
    }

    subscribe(fn: Function): void {
        this.user.subscribers.push(fn)
    }
}
