import { User } from './user'
import { Observer } from './observer'

const user = new User('Masha')
const observer = new Observer(user)

observer.subscribe((newUser) => {
    console.log(`obj changed, new name:${newUser._name}`)
})

setTimeout(() => (user.name = 'Varia'), 5000)
