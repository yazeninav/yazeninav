export class User {
    subscribers: Array<Function>
    _name: string

    constructor(name: string) {
        this.subscribers = []
        this._name = name
    }

    set name(name: string) {
        this._name = name
        this.publish(this)
    }

    publish(user: User): void {
        this.subscribers.forEach((func) => func(user))
    }
}
