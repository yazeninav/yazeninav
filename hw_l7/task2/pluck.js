let pluck = function () {

    if (arguments.length < 2) {
        console.log("incorrect args number ");
        return;
    }

    if (!(arguments[0] instanceof Array)) {
        console.log('first argument should be array');
        return;
    }

    const array = arguments[0];

    if (array.length === 0) {
        console.log('empty array');
        return array;
    }

    if (arguments.length === 2) {
        const arg = arguments[1];

        if (array[0][arg]) {
            return array.map(element => element[arg]);
        }

        return array;
    }

    const resultArray = []

    for (let i = 1; i < arguments.length; i++) {
        const arg = arguments[i];
        if (array[0][arg]) {
            array.map(element => element[arg]).forEach(element => resultArray.push(element));
        }
    }

    return resultArray;

}

module.exports = pluck;