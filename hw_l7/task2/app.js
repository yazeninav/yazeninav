const students = require('./students');
const pluck = require('./pluck');

console.log("show source array")
console.log(pluck(students,1))
console.log('create new array with names and ages')
console.log(pluck(students,'name','age'))