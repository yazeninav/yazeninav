let Card = require('./card');

function Atm() {
}

Atm.prototype.setCard = function (card) {
    if (card instanceof Card) {
        this.card = card;
    } else {
        console.log("this type of card does not supported")
    }
}

Atm.prototype.isCardInserted = function () {
    if (this.card) {
        console.log("Card not inserted ");
        return false;
    }
    return true;
}

Atm.prototype.withdrawBalance = function (amount) {
    if (this.isCardInserted()) {
        this.card.withdrawBalance(amount);
    }
}

Atm.prototype.supplementBalance = function (amount) {
    if (this.isCardInserted()) {
        this.card.supplementBalance(amount);
    }
}

module.exports=Atm;