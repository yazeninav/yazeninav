const Card = require('./card')

function CreditCard(ownerName,balance) {
    Card.call(this,ownerName,balance)
}

CreditCard.prototype = Object.create(Card.prototype)
CreditCard.prototype.constructor=CreditCard;
