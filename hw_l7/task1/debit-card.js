const Card = require('./card')

function DebitCard(ownerName, balance) {
    Card.call(this, ownerName, balance)
}

DebitCard.prototype = Object.create(Card.prototype);
DebitCard.prototype.constructor = DebitCard

DebitCard.prototype.withdrawBalance = function (amount) {

    if (this.getBalance() - amount < 0) {
        console.log("not enough money")
    } else {
        this.balance -= amount;
    }
}


let a = new DebitCard('fsefe', 123)

a.withdrawBalance(30)
console.log(a.getBalance())

module.exports=DebitCard


