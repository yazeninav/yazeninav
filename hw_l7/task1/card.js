function Card(ownerName, balance) {
    this.ownerName = ownerName;
    this.balance = balance || 0;
}

Card.prototype.getBalance = function () {
    return this.balance;
}
Card.prototype.withdrawBalance = function (amount) {
    this.balance -= amount;
}

Card.prototype.supplementBalance = function (amount) {
    if (typeof amount === 'number') {
        this.balance += amount;
    } else {
        console.log('incorrect input type')
    }
}
Card.prototype.getConvertedBalance = function (convertCoefficient) {
    return convertCoefficient * 1000 * this.balance / 1000;
}

module.exports = Card


