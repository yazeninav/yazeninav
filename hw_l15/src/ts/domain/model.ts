export abstract class Model {
  private static subsequence = 0;

  readonly id: number;

  constructor() {
    this.id = Model.subsequence++;
  }
}
