import { Track } from "./track/track";
import { Album } from "./album";
import { Artist } from "./artist";

export class MusicLibrary {
  [index: string]: any;

  private static instance: MusicLibrary;

  private constructor() {
    this.albums = [];
    this.artists = [];
    this.tracks = [];
  }

  static create() {
    if (this.instance == null) {
      return new MusicLibrary();
    }
    return this.instance;
  }

  tracks: Array<Track>;
  albums: Array<Album>;
  artists: Array<Artist>;
}
