import { AbstractCrudService } from "../api/abstract-crud-service";
import { Album } from "../../domain/album";
import { MusicLibrary } from "../../domain/music-library";

export class AlbumServiceImpl extends AbstractCrudService<Album> {
  constructor(musicLibrary: MusicLibrary) {
    super(musicLibrary);
  }
}
