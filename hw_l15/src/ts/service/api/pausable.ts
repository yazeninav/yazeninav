export interface Pausable {
  pause(): void;
}
