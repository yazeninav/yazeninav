import { Track } from "../../domain/track/track";

export interface Playable {
  play(track: Track): void;
}
