export interface Previousable {
  switchToPrevious(): void;
}
