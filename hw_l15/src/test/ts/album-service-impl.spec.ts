import "mocha";
import { expect } from "chai";
import { TestHelper } from "./test-helper";
import { MusicLibrary } from "../../ts/domain/music-library";
import { AlbumServiceImpl } from "../../ts/service/impl/album-service-impl";
import { Artist } from "../../ts/domain/artist";
import { Album } from "../../ts/domain/album";
import { Track } from "../../ts/domain/track/track";


describe("Album service test", () => {

  let artist: Artist;
  let album: Album;
  let track: Track;
  let musicLibrary: MusicLibrary;
  let albumServiceImpl: AlbumServiceImpl;

  beforeEach(done => {
    artist = TestHelper.getArtist();
    album = TestHelper.getAlbum();
    track = TestHelper.getTrack();
    artist.albums = [album];
    album.tracks = [track];
    artist.albums = [album];

    musicLibrary = MusicLibrary.create();
    musicLibrary.tracks = [track];
    musicLibrary.albums = [album];
    musicLibrary.artists = [artist];

    albumServiceImpl = new AlbumServiceImpl(musicLibrary);

    done();
  });

  // it("should add new album to music library", done => {
  //   albumServiceImpl.create(TestHelper.getAlbum());
  //   expect(musicLibrary.albums.length).to.equal(2);
  //   done()
  // });

  describe("#create", function() {
    it("should add new album to music library", done => {
      albumServiceImpl.create(TestHelper.getAlbum());
      expect(musicLibrary.albums.length).to.equal(2);
      done();
    });
  });

  describe("#delete", function() {
    it("should remove album from music library", done => {
      albumServiceImpl.delete(album);
      expect(musicLibrary.albums.length).to.equal(0);
      done();
    });
  });

  describe("#read", function() {
    it("should return album from music library", done => {
      expect(albumServiceImpl.read(album)).to.equal(album);
      done();
    });

    it("should throw error", done => {
      expect(()=>albumServiceImpl.read(TestHelper.getAlbum())).to.throw;
      done();
    });
  });

  describe("#update", function() {
    it("should update exist album", done => {
      album.name='new name';
      albumServiceImpl.update(album);
      expect(albumServiceImpl.read(album).name).to.equal('new name');
      done();
    });

    it("should create new album if album not exist", done => {
      album.name='new name';
      albumServiceImpl.update(TestHelper.getAlbum());
      expect(musicLibrary.albums.length).to.equal(2);
      done();
    });
  });
});