import "mocha";
import { expect } from "chai";
import { TestHelper } from "./test-helper";
import { MusicLibrary } from "../../ts/domain/music-library";
import { Artist } from "../../ts/domain/artist";
import { Album } from "../../ts/domain/album";
import { Track } from "../../ts/domain/track/track";
import { TrackServiceImpl } from "../../ts/service/impl/track-service-impl";
import { TrackState } from "../../ts/domain/track/track-state";


describe("Album service test", () => {

  let artist: Artist;
  let album: Album;
  let track: Track;
  let musicLibrary: MusicLibrary;
  let trackServiceImpl: TrackServiceImpl;

  beforeEach(done => {
    artist = TestHelper.getArtist();
    album = TestHelper.getAlbum();
    track = TestHelper.getTrack();
    artist.albums = [album];
    album.tracks = [track];
    artist.albums = [album];

    musicLibrary = MusicLibrary.create();
    musicLibrary.tracks = [track];
    musicLibrary.albums = [album];
    musicLibrary.artists = [artist];

    trackServiceImpl = new TrackServiceImpl(musicLibrary);

    done();
  });

  describe("#create", function() {
    it("should add new track to music library", done => {
      trackServiceImpl.create(TestHelper.getTrack());
      expect(musicLibrary.tracks.length).to.equal(2);
      done();
    });
  });

  describe("#delete", function() {
    it("should remove track from music library", done => {
      trackServiceImpl.delete(track);
      expect(musicLibrary.tracks.length).to.equal(0);
      done();
    });
  });

  describe("#read", function() {
    it("should return track from music library", done => {
      expect(trackServiceImpl.read(track)).to.equal(track);
      done();
    });

    it("should throw error", done => {
      expect(() => trackServiceImpl.read(TestHelper.getTrack())).to.throw;
      done();
    });
  });

  describe("#update", function() {
    it("should update exist track", done => {
      track.name = "new name";
      trackServiceImpl.update(track);
      expect(trackServiceImpl.read(track).name).to.equal("new name");
      done();
    });

    it("should create new track if track not exist", done => {
      trackServiceImpl.update(TestHelper.getTrack());
      expect(musicLibrary.tracks.length).to.equal(2);
      done();
    });
  });

  describe("#pause", function() {
    it("should change exist track status to stop", done => {
      trackServiceImpl.play(track);
      trackServiceImpl.pause();
      expect(track.state).to.equal(TrackState.STOP);
      done();
    });

    it("should throw error if track has status stop", done => {
      trackServiceImpl.play(track);
      trackServiceImpl.pause();
      expect(() => trackServiceImpl.pause()).to.throw;
      done();
    });

    it("should throw error if track is not selected", done => {
      trackServiceImpl.play(track);
      trackServiceImpl.pause();
      expect(() => trackServiceImpl.pause()).to.throw;
      done();
    });
  });

  describe("#switch to next", function() {
    it("should switch to next track", done => {
      let nextTrack = TestHelper.getTrack();
      musicLibrary.tracks.push(nextTrack);
      trackServiceImpl.play(track);
      trackServiceImpl.switchToNext();
      expect(nextTrack.state).to.equal(TrackState.PLAY);
      done();
    });

    it("should throw error if track list empty", done => {
      musicLibrary.tracks.shift();
      expect(() => trackServiceImpl.switchToNext()).to.throw;
      done();
    });
  });

  describe("#switch to previous", function() {
    it("should switch to previous track", done => {
      let previousTrack = TestHelper.getTrack();
      musicLibrary.tracks.push(previousTrack);
      trackServiceImpl.play(track);
      trackServiceImpl.switchToPrevious();
      expect(previousTrack.state).to.equal(TrackState.PLAY);
      done();
    });

    it("should throw error if track list empty", done => {
      musicLibrary.tracks.shift();
      expect(() => trackServiceImpl.switchToPrevious()).to.throw;
      done();
    });
  });

  describe("#play", function() {
    it("should play track", done => {
      trackServiceImpl.play(track);
      expect(track.state).to.equal(TrackState.PLAY);
      done();
    });

    it("should throw error if track is playing", done => {
      trackServiceImpl.play(track);
      expect(() => trackServiceImpl.play(track)).to.throw;
      done();
    });

    it("should throw error if another track is playing", done => {
      trackServiceImpl.play(TestHelper.getTrack());
      expect(() => trackServiceImpl.play(track)).to.throw;
      done();
    });
  });
});