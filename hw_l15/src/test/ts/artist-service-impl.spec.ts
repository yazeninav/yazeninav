import "mocha";
import { expect } from "chai";
import { TestHelper } from "./test-helper";
import { MusicLibrary } from "../../ts/domain/music-library";
import { Artist } from "../../ts/domain/artist";
import { Album } from "../../ts/domain/album";
import { Track } from "../../ts/domain/track/track";
import { ArtistServiceImpl } from "../../ts/service/impl/artist-service-impl";


describe("Artist service test", () => {

  let artist: Artist;
  let album: Album;
  let track: Track;
  let musicLibrary: MusicLibrary;
  let artistServiceImpl: ArtistServiceImpl;

  beforeEach(done => {
    artist = TestHelper.getArtist();
    album = TestHelper.getAlbum();
    track = TestHelper.getTrack();
    artist.albums = [album];
    album.tracks = [track];
    artist.albums = [album];

    musicLibrary = MusicLibrary.create();
    musicLibrary.tracks = [track];
    musicLibrary.albums = [album];
    musicLibrary.artists = [artist];

    artistServiceImpl = new ArtistServiceImpl(musicLibrary);

    done();
  });


  describe("#create", function() {
    it("should add new artist to music library", done => {
      artistServiceImpl.create(TestHelper.getArtist());
      expect(musicLibrary.artists.length).to.equal(2);
      done();
    });
  });

  describe("#delete", function() {
    it("should remove artist from music library", done => {
      artistServiceImpl.delete(artist);
      expect(musicLibrary.artists.length).to.equal(0);
      done();
    });
  });

  describe("#read", function() {
    it("should return artist from music library", done => {
      expect(artistServiceImpl.read(artist)).to.equal(artist);
      done();
    });

    it("should throw error", done => {
      expect(() => artistServiceImpl.read(TestHelper.getArtist())).to.throw;
      done();
    });
  });

  describe("#update", function() {
    it("should update exist artist", done => {
      artist.firstName = "new name";
      artistServiceImpl.update(artist);
      expect(artistServiceImpl.read(artist).firstName).to.equal("new name");
      done();
    });

    it("should create new artist if artist not exist", done => {
      artistServiceImpl.update(TestHelper.getArtist());
      expect(musicLibrary.artists.length).to.equal(2);
      done();
    });
  });
});