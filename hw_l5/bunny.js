const getMonthName = function (monthNumber) {
    switch (monthNumber) {
        case 1:
            return 'January';
        case 2:
            return 'February';
        case 3:
            return 'March';
        case 4:
            return 'April';
        case 5:
            return 'May';
        case 6:
            return 'June';
        default:
            return 'incorrect month number';
    }
};

const MONTH_AMOUNT = 6;
const GROWTH_IN_A_MONTH = 20;

let bunnyAmount = 200;

for (let i = 1; i <= MONTH_AMOUNT; i++) {

    if (i % 2 === 0) {
        bunnyAmount /= 2
    } else {
        if (bunnyAmount > 100) {
            bunnyAmount -= bunnyAmount / 5
        }

    }
    bunnyAmount += GROWTH_IN_A_MONTH;

    console.log('Month: ' + getMonthName(i) + ', ' + 'bunny amount: ' + bunnyAmount)
}

