const getFibonacci = function (bound) {

    let nextNumber = 1;
    let previousNumber = 0;
    let tempVariable;

    const result = {
        fibonacciSequence: previousNumber + " " + nextNumber,
        count: 0
    };

    while (previousNumber + nextNumber < bound) {
        tempVariable = nextNumber;
        nextNumber = nextNumber + previousNumber;
        previousNumber = tempVariable;

        result.fibonacciSequence += " " + nextNumber;
        result.count++;
    }

    return result

};

const result = getFibonacci(100);

console.log('fibonacci : ' + result.fibonacciSequence + '\n' + 'count :' + result.count);

