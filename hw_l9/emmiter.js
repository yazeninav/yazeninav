const EventEmitter = require('events').EventEmitter;
const Calculator = require('./calculator')

const calculator = new Calculator();
const eventEmitter = new EventEmitter();

eventEmitter.on('result', result => console.log('result: ' + result))
eventEmitter.on('log', msg => console.log(msg))

eventEmitter.on('calculate', async (event,arg1, arg2) => {
    const result = await calculator[event](arg1, arg2);
    eventEmitter.emit('log', `call ${event}`);
    eventEmitter.emit('result', result);
});

module.exports = eventEmitter;
