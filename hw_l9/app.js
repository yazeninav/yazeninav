const fs = require('fs');
const path = require('path');
const eventEmitter = require('./emmiter')

const stream = new fs.ReadStream(path.toNamespacedPath(path.resolve(__dirname,"input.txt")), {encoding: 'utf-8'});

stream.on('readable', function () {
    const data = stream.read();
    if (data != null) {
        const lines = data.split(/\r?\n/);
        lines.forEach(line => emitEvent(line))
    }
});

function emitEvent(data) {
    const args = data.split(" ")

    function parseNumber(value) {
        const number = Number(args[0]);
        if (isNaN(number)) throw `incorrect input value:${value}`
        return number;
    }

    const firstArg = parseNumber(args[0]);
    const secondArg = parseNumber(args[1]);
    const eventType = args[2]

    switch (eventType) {
        case '+':
            eventEmitter.emit('calculate', 'sum',firstArg, secondArg);
            break;
        case '-':
            eventEmitter.emit('calculate', 'minus',firstArg, secondArg);
            break;
        case '/':
            eventEmitter.emit('calculate', 'divide',firstArg, secondArg);
            break;
        case '*':
            eventEmitter.emit('calculate', 'multiply',firstArg, secondArg);
            break;
        default:
            console.log(`not supported event:${eventType}`);
    }
}
