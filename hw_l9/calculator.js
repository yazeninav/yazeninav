class Calculator {

    sum(arg1, arg2) {
        return this.calculateWithDelay(arg1 + arg2);
    }

    minus(arg1, arg2) {
        return this.calculateWithDelay(arg1 - arg2)
    }

    divide(arg1, arg2) {
        return this.calculateWithDelay(arg1 / arg2)
    }

    multiply(arg1, arg2) {
        return this.calculateWithDelay(arg1 * arg2)
    }

    calculateWithDelay(value) {
        return new Promise(resolve => {
            setTimeout(function () {
                resolve(value);
            }, 15);
        });
    }

}

module.exports = Calculator;