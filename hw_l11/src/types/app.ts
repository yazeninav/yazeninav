import {Observer} from "./observer";

const user = {name: 'Masha'};
const observer = new Observer(user)
    .filter(newUser => newUser.name != null)
    .map(newUser => newUser.name.toUpperCase());

observer.subscribe(newName => {
    console.log(`obj changed, new name:${newName}`);
});

setTimeout(() => {
    user.name = 'Varia';
}, 100);
