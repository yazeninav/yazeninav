export class Observer {
    user: object
    subscribers:Array<Function>
    operators : Array<any>

    constructor(user:object) {
        this.user=user;
        this.subscribers = [];
        this.operators = [];
        let copy = Object.assign({},user);

        setInterval(() => {
            if (!Object.is(copy,this.user)||!this.user){
            this.notify(this.user)
            process.exit(0);}
        }, 300);
    }

    notify(data) {
        if (this.subscribers.length === 0) return;
        for (const operator of this.operators) {
            if (operator.name === 'filter') {
                if (!operator.fn(data)) return;
            }
            if (operator.name === 'map') {
                data = operator.fn(data);
            }
        }
        for (const observer of this.subscribers) {
            observer(data);
        }
    }

    filter(predicate) {
        this.operators.push({ name: 'filter', fn: predicate });
        return this;
    }

    map(callback) {
        this.operators.push({ name: 'map', fn: callback });
        return this;
    }

    subscribe(fn: Function): void {
        this.subscribers.push(fn)
    }
}