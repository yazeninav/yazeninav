const yargs = require('yargs').argv;

exports.config = {

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    capabilities: {
        'browserName': 'chrome'
    },

    // Spec patterns are relative to this directory.
    specs: [
        '../features/*.feature'
    ],

    baseURL: 'localhost',
    directConnect: true,
    cucumberOpts: {
        require: 'features/step_definitions/stepDefinitions.js',
        tags: false,
        format: [],
        profile: false,
        'no-source': true
    },
    mochaOpts: {
        reporter: 'spec',
        timeout: 70000
    }
    // directConnect: true,
    //
    // framework: 'mocha',
    //
    // specs: [
    //     '../specs/*.js'
    // ],
    // capabilities: {
    //     'browserName': 'chrome',
    //
    // },
    // // shardTestFiles: yargs.instances > 1,
    // // maxInstances: yargs.instances || 2,
    // baseUrl: 'localhost',
    //
    // mochaOpts: {
    //     reporter: 'spec',
    //     timeout: 70000
    // }
};