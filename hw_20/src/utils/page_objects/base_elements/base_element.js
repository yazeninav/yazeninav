const logger = require('../../../config/logger.config');

class Element {

    constructor(elementName, selector) {

        this.element = element(by.css(selector));

        this.elementName = elementName;

    }

    click() {

        logger.info(`Clicking "${this.elementName}"`);

        return this.element.click();

    };

    async getText() {

        const elementText = await this.element.getText();

        logger.info(`"${this.elementName}" element text is ${elementText}`);

        return elementText;

    };

    async fillText(text) {

        const elementText = await this.element.sendKeys(text, protractor.Key.ENTER);

        logger.info(`"${this.elementName}" element text is ${text}`);

        return elementText;

    };

};


module.exports = Element;