let {Then} = require('cucumber');
let {When} = require('cucumber');
let {Given} = require('cucumber');
const expect = require("chai").expect;
const WelcomePage = require("../../utils/page_objects/welcome_page/welcome_page");
let {setDefaultTimeout} = require('cucumber');
browser.ignoreSynchronization = true;

setDefaultTimeout(60 * 1000);

let welcomePage;
let search;
let itemPage;
let cartPage;
Given(/^I go to https:\/\/ebay\.com\/$/, async function () {
    welcomePage = new WelcomePage();
    await welcomePage.open();
});
Given(/^I fill search input (.*)$/, async function (request) {
    welcomePage.wait(1000);
    search = await welcomePage.fillSearchInput(request);
});
Given(/^I click on the first search result$/, async function () {
    await search.wait(1000);
    itemPage = await search.clickOnSearchResult();
});
When(/^I click add to cart button$/, async function () {
    await itemPage.wait(1000);
    cartPage = await itemPage.addToCurt();
});
Then(/^I should see cart count increased$/, async function () {
    await cartPage.wait(2000);
    let result = await cartPage.element.getText();

    expect(result).to.be.equal("1");
});