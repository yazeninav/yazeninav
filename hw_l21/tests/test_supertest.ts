import {BASE_URL, API_PATH} from "src/constants";
import request from 'supertest';
import {expect} from "chai";

const USER_KEYS = ['id', 'email', 'first_name', 'last_name', 'avatar'];

describe(`Supertest`, () => {
    let requestBase = request(BASE_URL);
    it(`GET ${API_PATH.GET}`, (done) => {
        requestBase
            .get(API_PATH.GET)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                // expect(res.body.length).to.be.equal(100);
                return done();
            });
    })

    it(`GET ${API_PATH.GET}`, (done) => {
        requestBase
            .get(`${API_PATH.GET}/2`)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                USER_KEYS.forEach(key => expect(res.body.data[key]).to.not.be.undefined)
                return done();
            });
    })
});
