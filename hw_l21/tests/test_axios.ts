import {BASE_URL, API_PATH} from "src/constants";
import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import chai, {expect, use, assert} from "chai";
import chaiJsonSchema from 'chai-json-schema';
import * as postSchema from 'src/schemas/posts.schema.json';

use(chaiJsonSchema);
const should = chai.should();
const axiosConfig: AxiosRequestConfig = {
    baseURL: BASE_URL
}
const configWithContentTypeAppJson: AxiosRequestConfig = {
    ...axiosConfig, headers: {
        'Content-type': 'application/json'
    }
};

interface IPost {
    name: string,
    job: string

}

describe(`Axios test`, async () => {
    const instance = axios.create();
    it(`GET ${API_PATH.POSTS}`, async () => {
        const response: AxiosResponse<any> = await instance.get(`${API_PATH.GET}?page=2`, axiosConfig);
        expect(response.data.data).to.not.undefined;
        expect(response.data.data).to.be.an('array');
        expect(response.data.data.length).to.be.equal(6);
    });

    it(`PUT ${API_PATH.POSTS}/2`, async () => {
        const body: IPost = {
            name: "morpheus",
            job: "zion resident"

        }
        const res: AxiosResponse<IPost> = await instance.put(`${API_PATH.POSTS}/2`, body, configWithContentTypeAppJson)
        expect(res.status).to.be.equal(200);
        console.log(res.data);
        expect(res.data).to.be.jsonSchema(postSchema);

    });
    it(`DELETE ${API_PATH.POSTS}/2`, async () => {
        const res: AxiosResponse = await instance.delete(`${API_PATH.POSTS}/2`, axiosConfig);
        expect(res.status).to.be.equal(204);
    });

});
