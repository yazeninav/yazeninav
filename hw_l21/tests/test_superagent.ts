import { BASE_URL, API_PATH } from "src/constants";
import superagent from 'superagent';
import chai, { expect } from "chai";
const should = chai.should();
describe(`Superagent test`, async () => {
    it(`GET ${API_PATH.GET}`, async () => {
        try {
            const response = await superagent.get(`${BASE_URL}${API_PATH.GET}/2`);
            response.header['content-type'].should.to.not.undefined;
            response.header['content-type'].should.to.be.equal('application/json; charset=utf-8');
        } catch (ex) {
            console.log(ex)
        }
    });

    it(`POST ${API_PATH.POSTS}`, async () => {
        try {
            const response = await superagent.post(`${BASE_URL}${API_PATH.POSTS}`)
                .send({
                    "name": "morpheus",
                    "job": "leader"
                });
            expect(response.status).to.be.equal(201);
            console.log(response.body.id);
        } catch (ex) {
            console.log(ex)
        }
    });
});
