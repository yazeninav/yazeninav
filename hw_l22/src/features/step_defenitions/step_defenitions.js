const {Then,When,Given} = require('cucumber');
const expect = require("chai").expect;
const WelcomePage = require("../../utils/page_objects/welcome_page/welcome_page");
const {setDefaultTimeout} = require('cucumber');
const takeScreenShot = require('../../utils/screen_shot/screen_shot')
browser.ignoreSynchronization = true;
// const HighlightElement = require('protractor-highlight-elements');
// const h = new HighlightElement();

setDefaultTimeout(60 * 1000);

let welcomePage;
let search;
let itemPage;
let cartPage;

Given(/^I go to (.*)$/, async function (url) {
    welcomePage = new WelcomePage(url);
    await welcomePage.open();
    takeScreenShot();
});
Given(/^I fill search input (.*)$/, async function (request) {
    welcomePage.wait(1000);
    search = await welcomePage.fillSearchInput(request);
    takeScreenShot();
});
Given(/^I click on the first search result$/, async function () {
    await search.wait(1000);
    itemPage = await search.clickOnSearchResult();
    takeScreenShot();
});
When(/^I click add to cart button$/, async function () {
    await itemPage.wait(2000);
    cartPage = await itemPage.addToCurt();
    takeScreenShot();
});
Then(/^I should see cart count increased$/, async function () {
    await cartPage.wait(2000);
    let result = await cartPage.element.getText();
    await cartPage.highlightElement(cartPage.element.element)
    takeScreenShot();

    expect(result).to.be.equal("1");
});
