Feature: Protractor and Cucumber Test

  Scenario Outline: Add item in cart
    Given I go to "https://ebay.com/"
    And I fill search input <request>
    And I click on the first search result
    When I click add to cart button
    Then I should see cart count increased

    Examples:
      | request  |
      | iphone x |
