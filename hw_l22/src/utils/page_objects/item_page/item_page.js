const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Element = require("../base_elements/base_element");
const CartPage = require("../cart_page/cart_page");

class ItemPage extends BasePage {
    constructor() {
        super();
        this.element = new Element("Add to cart", "#isCartBtn_btn");
    };

    async addToCurt() {
        await this.element.click();
        return new CartPage();
    }
};

module.exports = ItemPage;