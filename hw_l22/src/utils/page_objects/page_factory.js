const WelcomePage = require("./welcome_page/welcome_page");
const BasePage = require("./base_page/base_page");

class PageFactory {
     static getPage(pageName) {
        switch (pageName) {
            case "Welcome":
                return new WelcomePage();
            default:
                return new BasePage();
        }
        ;
    };
};

module.exports = PageFactory;