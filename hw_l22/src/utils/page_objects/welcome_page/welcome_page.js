const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Element = require("../base_elements/base_element")
const SearchPage = require("../search_page/search_page")

class WelcomePage extends BasePage {
    constructor(url) {
        super();
        this.url = "https://www.ebay.com/";
        this.searchInput = new Element("Search input", "#gh-ac.gh-tb.ui-autocomplete-input");
    };

    async open() {
        return super.open(this.url);
    };

    async fillSearchInput(request) {
        await this.searchInput.fillText(request);
        return new SearchPage();
    }
};

module.exports = WelcomePage;