const logger = require('../../../config/logger.config');
const BasePage = require("../base_page/base_page");
const Element = require("../base_elements/base_element");

class CartPage extends BasePage {
    constructor() {
        super();
        this.element = new Element("Cart item count to cart", "#gh-cart-n");
    };

};

module.exports = CartPage;