const logger = require('../../../config/logger.config');

class BasePage {
    constructor() {
    };
    wait(waitInMilliseconds) {
        logger.info(`Waiting "${waitInMilliseconds}" milliseconds`);
        return browser.sleep(waitInMilliseconds);
    };

    async open(url) {
        logger.info(`Opening "${url}" url`);
        return browser.get(url);
    };

   async highlightElement(el){
        logger.info("highlight--");

        return browser.driver.executeScript("arguments[0].setAttribute('style', arguments[1]);",el.getWebElement(), "color: Yellow; border: 4px solid yellow;");

    };
};

module.exports = BasePage;