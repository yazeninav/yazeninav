const BasePage = require("../base_page/base_page");
const Element = require("../base_elements/base_element");
const Collection = require("../base_elements/base_collection");
const ItemPage = require("../item_page/item_page");

class SearchPage extends BasePage {
    constructor() {
        super();
        this.searchResult= new Element("search_result",".s-item__title")
    };

    async clickOnSearchResult() {
        await this.searchResult.click();
        return new ItemPage()
    }

};

module.exports = SearchPage;