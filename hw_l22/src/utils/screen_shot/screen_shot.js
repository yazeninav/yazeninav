const fs = require('fs');
let counter=0;

function writeScreenShot(data, filename) {
    let stream = fs.createWriteStream(filename);
    stream.write(new Buffer(data, 'base64'));
    stream.end();
}

function getCurrentDate() {
    let today = new Date();
    let dd = today.getDate();
    let time =today.getTime()

    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    return mm + '-' + dd + '-' + yyyy+" : "+time;
}

let takeScreenShot = function () {
    browser.takeScreenshot().then(function (png) {
        writeScreenShot(png, `${getCurrentDate()}.png`);
    });
}

module.exports = takeScreenShot;