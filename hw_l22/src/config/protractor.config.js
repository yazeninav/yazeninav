exports.config = {

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    capabilities: {
        'browserName': 'chrome'
    },

    specs: [
        '../features/*.feature'
    ],

    baseURL: 'localhost',
    directConnect: true,

    cucumberOpts: {
        require: 'features/step_definitions/stepDefinitions.js',
        strict: true,
        format: [],
        'dry-run': false,
        compiler: [],
        format: 'json:results.json'
    },
    SELENIUM_PROMISE_MANAGER: false,

    mochaOpts: {
        reporter: 'spec',
        timeout: 70000
    },
    onComplete: () => {
        var reporter = require('cucumber-html-reporter');
        var options = {
            theme: 'bootstrap',
            jsonFile: './results.json',
            output: './results.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "App Version": "0.3.2",
                "Test Environment": "STAGING",
                "Browser": "Chrome  83.0.4103.106",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            },
            output: './report/cucumber_report.html',
        };
        reporter.generate(options);
    }

};